import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from './components/Video';
import DetailedVideo from './components/DetailedVideo';
import Channel from './components/Channel';
import DetailedChannel from './components/DetailedChannel';
// import SearchResult from './components/SearchResult';
// import DetailedResult from './components/DetailedResult';
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

// Fat Arrow
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fl%C3%A9ch%C3%A9es
const App = () => {
    const [type, setType] = useState("");

    // const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState({});
    const [selectedChannel, selectChannel] = useState({});

    const [results, setResults] = useState([]);
    // const [selectedResult, selectResult] = useState({});


    return (
        <Container className="p-3">
            <Router>
                {/* <MyNav onResults={setVideos} onSearchType={setType} type={type} /> */}

                <MyNav onResults={setResults} onSearchType={setType} type={type} />

                <div> {type === "" ? "Veuillez cliquer sur le bouton Vidéos ou Channel si dessus." : ""}</div>

                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedVideo id={selectedVideo.id}
                                       snippet={selectedVideo.snippet} player={selectedVideo.player}
                                       statistics={selectedVideo.statistics} />
                    </Route>
                    <Route path="/search/videos/:search">
                        <>
                            {results.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                return (<Video
                                    videoId={v.id.videoId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectVideo={selectVideo}/>);
                            })}
                        </>
                    </Route>

                    <Route path="/channels/:videoId">
                        <DetailedChannel id={selectedChannel.id}
                                       snippet={selectedChannel.snippet}
                                       statistics={selectedChannel.statistics} />
                    </Route>
                    <Route path="/search/channels/:search">
                        <>
                            {results.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                return (<Channel
                                    channelId={v.id.channelId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectChannel={selectChannel}/>);
                            })}
                        </>
                    </Route>

                    {/* <Route path="/:type/:resultId">
                        <DetailedResult id={selectedResult.id}
                                       snippet={selectedResult.snippet} player={selectedResult.player}
                                       statistics={selectedResult.statistics} />
                    </Route>
                    <Route path="/search/:type/:search">
                        <>
                            {results.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                return (<SearchResult
                                    type={type}
                                    videoId={v.id.videoId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectResult={selectResult}/>);
                            })}
                        </>
                    </Route>
                     */}

                    <Route path="/">
                    Merci d'effectuer une recherche...
                </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;

