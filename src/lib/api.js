import axios from 'axios'

// const key = 'AIzaSyCKfQzhXlXPaNByHYwjDfxxIvH3odu6R4k'
// const key = 'AIzaSyCUo4rIUwXjxm0rbjvimuSURYJni07k8GI'
// const key = 'AIzaSyBED9P_ax8oVrubW_rzYYn4sVqXo53G9YM'
// const key = 'AIzaSyBpqFUi9C4hvGUhQgrur0M2vfH8DpAKrOY'
const key = 'AIzaSyAZsjBdSMJ7vseCLAUyEB89Mg8hjYJ7o7U'

const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key // On peut simplifier avec juste "key", c'est possible d'utiliser ce raccourci quand clé-valeur sont identiques.
    }
})

export default fetcher;