// import {useState} from 'react';
import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';

const MyNav = ({onResults, onSearchType, type}) => {
    return (<Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Reactube</Navbar.Brand>
        <Nav className="mr-auto">
            <Nav.Link onClick={() => onSearchType("video")} href="#videos">Videos</Nav.Link>
            <Nav.Link onClick={() => onSearchType("channel")} href="#channels">Channels</Nav.Link>
        </Nav>
        <SearchBar onResults={onResults} type={type} />
    </Navbar>);
}

export default MyNav;
