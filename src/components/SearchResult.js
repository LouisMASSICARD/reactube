import React from 'react';
import Card from 'react-bootstrap/Card';
import {Col, Image, Row} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";

const Video = ({type, id, thumbnail, title, description, channelTitle, publishTime, selectResult}) => {
    const history = useHistory();
    const search = async () => {
        let part
        switch (type) {
        case "video":
            part = 'snippet,statistics,contentDetails,player,recordingDetails,topicDetails'
            break;

        case "channel":
            part = 'snippet,statistics,contentDetails'
            break;
        
        default:
            break;
        }
        const resp = await api.get('/'+type+"s", {
            params: {
                id: id,
                part: part,
            }
        });
        console.log('Received', resp.data.items);
        selectResult(resp.data.items[0]);
        history.push(`/${type}s/${id}`);
    };

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Row>
                <Col xs={3}>
                    <Image src={thumbnail.url}
                           fluid={true}
                           rounded
                           onClick={search}/>
                </Col>
                <Col>
                    <Card.Title onClick={search}>{title}</Card.Title>
                    <Card.Subtitle>{channelTitle}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className={"mb-1 text-muted"}>
                        {publishTime}
                    </Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>)
};

export default Video;
