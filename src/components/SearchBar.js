import React, {useState} from 'react';
import {Button, Form, FormControl} from "react-bootstrap";
import api from '../lib/api';
import {useHistory} from "react-router-dom";

const SearchBar = ({onResults, type}) => {
    const [typed, setTyped] = useState('');
    const history = useHistory();

    const search = async () => {
        switch (type) {
        case "video":
            console.log("You search some videos")
            break;
        case "channel":
            console.log("You search some channels")
            break;
        default:
            console.log("Please click on button !");
            break;
        }

        if (type !== "") {
            const resp = await api.get('/search', {
                params: {
                    q: typed,
                    part: 'snippet',
                    maxResults: 10,
                    type: type
                }
            });
            console.log('Received', resp.data.items);
            onResults(resp.data.items);
            history.push(`/search/${type}s/${typed}`);
        }
    };

    return (<Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2"
                     value={typed}
                     onChange={event => setTyped(event.target.value)}
        />
        <Button variant="outline-info" onClick={search}>Search</Button>
    </Form>);
}

export default SearchBar;
