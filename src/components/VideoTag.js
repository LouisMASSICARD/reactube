import {Badge} from "react-bootstrap";

const VideoTag = ({index, tag}) => {
    return (<Badge variant={index % 2  ? "dark" : "info"}>{tag}</Badge>);
}

export default VideoTag;
